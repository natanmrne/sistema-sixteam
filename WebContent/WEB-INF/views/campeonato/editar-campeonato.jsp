<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
<c:import url="../head.jsp" />
</head>
<body>
	<!-- NAVBAR START -->
	<c:import url="../menu.jsp" />
	<!-- NAVBAR END -->
	<!-- Container -->
	<div class="container p-5 size">
		<div class="row">
			<div class="col mb-3">
				<h1 class="title text-center">Editar ${campeonato.nome}</h1>
				<div class="title-underline bg-success"></div>
				<p class="text-center font-italic font-weight-light my-3 display-6">Informe
					os novos dados do campeonato</p>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="erros d-flex justify-content-center align-items-center">
					<form:errors path="campeonato.nome" cssStyle="color:red;font-size:14px"/>
					<form:errors path="campeonato.regra.dataInicio" cssStyle="color:red;font-size:14px"/>
					<form:errors path="campeonato.regra.dataTermino" cssStyle="color:red;font-size:14px"/>
				</div>
				<form action="/sixteam/atualizar_campeonato" method="post">
					<input type="hidden" name="id" value="${campeonato.id}" />
					<input type="hidden" name="regra.id" value="${campeonato.regra.id}" />
					<div class="form-row">
						<div class="col">
							<div class="form-group">
								<label for="nome">Nome do Campeonato</label> <input type="text"
									id="nome" name="nome" class="form-control" value="${campeonato.nome}">
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col">
							<div class="form-group">
								<label for="categoria">Categoria</label> <select id="categoria"
									class="form-control" name="categoria.id"
									style="height: 2.076rem;">
									<c:forEach var="categoria" items="${categorias}">
										<c:if test="${ campeonato.categoria.id == categoria.id }">
											<option value="${categoria.id}" selected>${categoria.nome}</option>
										</c:if>
										<c:if test="${ campeonato.categoria.id != categoria.id }">
											<option value="${categoria.id}">${categoria.nome}</option>
										</c:if>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="col">
							<div class="form-group">
								<label for="tempoDeJogo">Tempo de Jogo</label> <input
									type="number" id="tempoJogo"
									name="regra.tempoJogo" class="form-control" min="5"
									max="45" onKeyPress="return false;" value="${campeonato.regra.tempoJogo}">
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col">
							<div class="form-group">
								<label for="quantidadeGrupos">Quantidade de Grupos</label> <input
									type="number" id="quantidadeGrupos"
									name="regra.quantidadeGrupos" class="form-control"
									min="1" max="26" value="${campeonato.regra.quantidadeGrupos}" onKeyPress="return false;">
							</div>
						</div>
						<div class="col">
							<div class="form-group">
								<label for="quantidadeDeTimes">Quantidade de Times <em>(Por grupo)</em></label> <input
									type="number" id="quantidadeTimes"
									name="regra.quantidadeTimes" class="form-control"
									min="1" max="30" value="${campeonato.regra.quantidadeTimes}" onKeyPress="return false;">
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col">
							<div class="form-group">
								<label for="pontosVitoria">Pontos por Vitória</label> <input
									type="number" id="pontosVitoria"
									name="regra.pontosVitoria" class="form-control"
									min="0" max="15" onKeyPress="return false;" value="${campeonato.regra.pontosVitoria}">
							</div>
						</div>
						<div class="col">
							<div class="form-group">
								<label for="pontosDerrota">Pontos por Derrota</label> <input
									type="number" id="pontosDerrota"
									name="regra.pontosDerrota" class="form-control"
									min="0" max="15" onKeyPress="return false;" value="${campeonato.regra.pontosDerrota}">
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col">
							<div class="form-group">
								<label for="pontosEmpate">Pontos por Empate</label> <input
									type="number" id="pontosEmpate"
									name="regra.pontosEmpate" class="form-control"
									min="0" max="15" onKeyPress="return false;" value="${campeonato.regra.pontosEmpate}">
							</div>
						</div>
						<div class="col">
							<div class="form-group">
								<label for="pontosEmpateZero">Pontos por Empate (Sem
									Saldo)</label> <input type="number"
									id="pontosEmpateZero" name="regra.pontosEmpateZero"
									class="form-control" min="0" max="15"
									onKeyPress="return false;" value="${campeonato.regra.pontosEmpateZero}">
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col">
							<div class="form-group">
							<fmt:formatDate value="${campeonato.regra.dataInicio}" type="date" pattern="dd/MM/yyyy" var="dataFormatadaInicio" />
								<label for="dataInicio">Data de Início</label> <input
									id="dataInicio" name="regra.dataInicio"
									class="form-control" onKeyPress="return false;" value="${dataFormatadaInicio}" />
							</div>
						</div>
						<div class="col">
							<div class="form-group">
							<fmt:formatDate value="${campeonato.regra.dataTermino}" type="date" pattern="dd/MM/yyyy" var="dataFormatadaTermino" />
								<label for="dataTermino">Data de Término</label> <input
									id="dataTermino" name="regra.dataTermino"
									class="form-control" onKeyPress="return false;" value="${dataFormatadaTermino}"/>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col d-flex justify-content-center align-items-center">
							<button type="submit" class="btn btn-success btn-lg mt-3">Salvar</button>
							<a href="#" onclick="history.go(-1)" class="btn btn-secondary btn-lg mt-3">Cancelar</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Fim Container -->

	<!-- Modal Login -->
	<c:import url="../login-modal.jsp" />
	<!-- footer -->
	<footer id="footer" class="bg-dark py-3 flex-shrink-0">
		<div class="container">
			<div class="row flex-column align-items-center">
				<div class="col-6 text-center">
					<a href="#" class="btn"> <i
						class="fab fa-facebook fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-twitter fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-instagram fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-google-plus fa-3x text-white m-2"></i>
					</a>
				</div>
				<div class="col-6">
					<p class="text-center text-bold text-white">2018 © sixTeam</p>
				</div>
			</div>
		</div>
	</footer>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
		crossorigin="anonymous"></script>
	<script
		src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js"
		integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9"
		crossorigin="anonymous"></script>
	
	<!-- Datepicker JS -->
	<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js"
		type="text/javascript"></script>
	<script>
		$(document).ready(function() {
			$('body').bootstrapMaterialDesign();
			//let params = new URLSearchParams(window.location.search);
			$('[data-toggle="tooltip"]').tooltip();

			// Instancia DateInput para Data de Início
			$('#dataInicio').datepicker({
				showOtherMonths : true,
				format : 'dd/mm/yyyy'
			});

			// Instancia DateInput para Data de Término
			$('#dataTermino').datepicker({
				showOtherMonths : true,
				format : 'dd/mm/yyyy'
			});
		});
	</script>
</body>
</html>