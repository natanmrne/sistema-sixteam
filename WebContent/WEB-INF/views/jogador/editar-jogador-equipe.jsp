<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<c:import url="../head.jsp" />
</head>
<body>
	<!-- NAVBAR START -->
	<c:import url="../menu.jsp" />
	<!-- NAVBAR END -->
	<!-- Container -->
	<div class="container p-5 size">
		<div class="row">
			<div class="col mb-3">
				<h1 class="title text-center">Editar ${timeJogador.jogador.nome}</h1>
				<div class="title-underline bg-success"></div>
				<p class="text-center font-italic font-weight-light my-3 display-6">Informe
					os dados do jogador</p>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<form action="/sixteam/atualizar_jogador_equipe" method="post">
					<input type="hidden" name="id" value="${timeJogador.id}" />
					<input type="hidden" name="time.id" value="${timeJogador.time.id}" />
					<input type="hidden" name="jogador.id" value="${timeJogador.jogador.id}" />
					<div class="form-row">
						<div class="col">
							<div class="form-group">
								<label for="numero">Número</label> <input type="number"
									id="numero" name="numero" class="form-control" min="1" max="35"
									value="${timeJogador.numero}" onKeyPress="return false;">
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col">
							<div class="form-group">
								<label for="posicao">Posição</label> <select id="categoria"
									class="form-control" name="posicao.id" style="height: 2.076rem;">
									<c:forEach var="posicao" items="${posicoes}">
										<c:if test="${ posicao.id == timeJogador.posicao.id }">
											<option value="${posicao.id}" selected>${posicao.posicao}</option>
										</c:if>
										<c:if test="${ posicao.id != timeJogador.posicao.id }">
											<option value="${posicao.id}">${posicao.posicao}</option>
										</c:if>
									</c:forEach>
								</select>
							</div>
						</div>
					</div>
					<fieldset class="form-group">
						<div class="row">
							<legend class="col-form-label col-sm-2 pt-0">Função</legend>
							<div class="col-sm-10">
								<c:if test="${ timeJogador.titular == true }">
									<div class="form-check">
										<input class="form-check-input" type="radio" name="titular" id="titular" value="1" checked>
										<label class="form-check-label" for="titular"> Titular </label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="titular" id="reserva" value="0">
										<label class="form-check-label" for="reserva"> Reserva </label>
									</div>
								</c:if>
								<c:if test="${ timeJogador.titular == false }">
									<div class="form-check">
										<input class="form-check-input" type="radio" name="titular" id="titular" value="1">
										<label class="form-check-label" for="titular"> Titular </label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="titular" id="reserva" value="0" checked>
										<label class="form-check-label" for="reserva"> Reserva </label>
									</div>
								</c:if>
							</div>
						</div>
					</fieldset>
					<div class="form-group row">
						<div class="col-sm-2">Capitão?</div>
						<div class="col-sm-10">
							<div class="form-check">
								<c:if test="${ timeJogador.capitao == true }">
									<input class="form-check-input" type="checkbox" id="capitao" name="capitao" checked>
								</c:if>
								<c:if test="${ timeJogador.capitao == false }">
									<input class="form-check-input" type="checkbox" id="capitao" name="capitao">
								</c:if>
								<label class="form-check-label" for="capitao"> Sim </label>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col d-flex justify-content-center">
							<button type="submit" class="btn btn-success btn-lg mt-3">Salvar</button>
							<a href="#" onclick="history.go(-1)" class="btn btn-secondary btn-lg mt-3">Cancelar</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Fim Container -->

	<!-- Modal Login -->
	<c:import url="../login-modal.jsp" />
	<!-- footer -->
	<footer id="footer" class="bg-dark py-3 flex-shrink-0">
		<div class="container">
			<div class="row flex-column align-items-center">
				<div class="col-6 text-center">
					<a href="#" class="btn"> <i
						class="fab fa-facebook fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-twitter fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-instagram fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-google-plus fa-3x text-white m-2"></i>
					</a>
				</div>
				<div class="col-6">
					<p class="text-center text-bold text-white">2018 © sixTeam</p>
				</div>
			</div>
		</div>
	</footer>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
		crossorigin="anonymous"></script>
	<script
		src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js"
		integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9"
		crossorigin="anonymous"></script>
	
	<!-- Datepicker JS -->
	<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js"
		type="text/javascript"></script>
	<script>
		$(document).ready(function() {
			$('body').bootstrapMaterialDesign();
			//let params = new URLSearchParams(window.location.search);
			$('[data-toggle="tooltip"]').tooltip();

			// Instancia DateInput para Data de Início
			$('#dataInicio').datepicker({
				showOtherMonths : true,
				format : 'dd/mm/yyyy'
			});

			// Instancia DateInput para Data de Término
			$('#dataTermino').datepicker({
				showOtherMonths : true,
				format : 'dd/mm/yyyy'
			});
		});
	</script>
</body>
</html>