<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<c:import url="../head.jsp" />
</head>
<body>
	<!-- NAVBAR START -->
	<c:import url="../menu.jsp" />
	<!-- NAVBAR END -->
	<!-- Container -->
	<div class="container py-5 size">
		<div class="row">
			<div class="col mb-3">
				<h1 class="title text-center">Jogador</h1>
				<div class="title-underline bg-success"></div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="card card-jogador">
					<img class="card-img-left img-fluid"
						src="/sixteam/${timeJogador.jogador.foto}"
						alt="">
					<div class="card-body p-0" style="width: 400px;">
						<div class="row justify-content-center">
							<div class="col text-center">
								<h2 class="mt-2 mb-0">${timeJogador.jogador.nome}</h2>
								<h6 class="font-italic">${timeJogador.jogador.categoria.nome}</h6>
							</div>
						</div>
						<div class="row justify-content-center align-items-center h-75">
							<div class="card-group align-items-center text-center">
								<a href="#gols">
									<div class="card-overflow d-flex justify-content-center align-items-center">
										<i class="fas fa-eye fa-5x text-white"></i>
									</div>
									<div class="card">
										<i class="card-img-top fas fa-futbol fa-4x mt-3"></i>
										<div class="card-body">
											<h5>Gols</h5>
											<p>${listaGols.size()}</p>
										</div>
									</div>
								</a>
								<a href="#amarelos">
									<div
										class="card-overflow d-flex justify-content-center align-items-center">
										<i class="fas fa-eye fa-5x text-white"></i>
									</div>
									<div class="card">
										<i class="card-img-top fas fa-mobile fa-4x mt-3 text-yellow"></i>
										<div class="card-body">
											<h5>Amarelos</h5>
											<p>${listaAmarelos.size()}</p>
										</div>
									</div>
								</a> <a href="#vermelhos">
									<div
										class="card-overflow d-flex justify-content-center align-items-center">
										<i class="fas fa-eye fa-5x text-white"></i>
									</div>
									<div class="card">
										<i class="card-img-top fas fa-mobile fa-4x mt-3 text-danger"></i>
										<div class="card-body">
											<h5>Vermelhos</h5>
											<p>${listaVermelhos.size()}</p>
										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row justify-content-center mt-4">
			<div class="col-6">
				<div class="card">
					<div class="card-header text-center py-0">
						<h5 class="card-title my-2">Sumário</h5>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col font-weight-bold text-right estatisticas">
								<p>Nome</p>
								<p>Categoria</p>
								<p>Equipe</p>
								<p>Posição</p>
								<p>Gols</p>
								<p>Cartões Amarelos</p>
								<p>Cartões Vermelhos</p>
							</div>
							<div class="col text-left estatisticas">
								<p>${timeJogador.jogador.nome}</p>
								<p>${timeJogador.jogador.categoria.nome}</p>
								<p>${timeJogador.time.time.nome}</p>
								<p>${timeJogador.posicao.posicao}</p>
								<p>${listaGols.size()}</p>
								<p>${listaAmarelos.size()}</p>
								<p>${listaVermelhos.size()}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<section id="gols">
					<div class="row mt-5">
						<div class="col">
							<h1 class="title text-center">Gols</h1>
							<div class="title-underline bg-success"></div>
						</div>
					</div>
					<div class="row mt-2 justify-content-center">
						<div class="table-responsive-md mt-3 w-100">
							<table class="table text-center">
								<thead class="thead-dark">
									<tr>
										<th scope="col">#</th>
										<th scope="col">Campeonato</th>
										<th scope="col">Equipe</th>
										<th scope="col">Tipo de Gol</th>
										<th scope="col">Tempo</th>
									</tr>
								</thead>
								<tbody>
								<c:if test="${not empty listaGols}" >
									<c:forEach var="gol" items="${listaGols}">
										<tr class="table-align-middle">
											<td>${gol.id}</td>
											<td>${gol.jogo.campeonato.nome}</td>
											<td>${gol.time.time.nome}</td>
											<td>${gol.acao.descricao}</td>
											<td>${gol.tempo} minuto(s)</td>
										</tr>
									</c:forEach>
								</c:if>
								<c:if test="${empty listaGols}">
									<tr class="table-align-middle">
										<td colspan="6">Nenhum gol registrado</td>
									</tr>
								</c:if>
								</tbody>
							</table>
						</div>
					</div>
				</section>
				<section id="amarelos">
					<div class="row mt-5">
						<div class="col">
							<h1 class="title text-center">Cartões Amarelos</h1>
							<div class="title-underline bg-success"></div>
						</div>
					</div>
					<div class="row mt-2 justify-content-center">
						<div class="table-responsive-md mt-3 w-100">
							<table class="table text-center">
								<thead class="thead-dark">
									<tr>
										<th scope="col">#</th>
										<th scope="col">Campeonato</th>
										<th scope="col">Tempo</th>
									</tr>
								</thead>
								<tbody>
									<c:if test="${not empty listaAmarelos}">
										<c:forEach var="cartaoAmarelo" items="${listaAmarelos}">
											<tr class="table-align-middle">
												<td>${cartaoAmarelo.id}</td>
												<td>${cartaoAmarelo.jogo.campeonato.nome}</td>
												<td>${cartaoAmarelo.tempo} minuto(s)</td>
											</tr>
										</c:forEach>
									</c:if>
									<c:if test="${empty listaAmarelos}">
										<tr class="table-align-middle">
											<td colspan="3">Nenhum cartão amarelo registrado</td>
										</tr>
									</c:if>
								</tbody>
							</table>
						</div>
					</div>
				</section>
				<section id="vermelhos">
					<div class="row mt-5">
						<div class="col">
							<h1 class="title text-center">Cartões Vermelhos</h1>
							<div class="title-underline bg-success"></div>
						</div>
					</div>
					<div class="row mt-2 justify-content-center">
						<div class="table-responsive-md mt-3 w-100">
							<table class="table text-center">
								<thead class="thead-dark">
									<tr>
										<th scope="col">#</th>
										<th scope="col">Campeonato</th>
										<th scope="col">Tempo</th>
									</tr>
								</thead>
								<tbody>
									<c:if test="${not empty listaVermelhos}">
										<c:forEach var="cartaoVermelho" items="${listaVermelhos}">
											<tr class="table-align-middle">
												<td>${cartaoVermelho.id}</td>
												<td>${cartaoVermelho.jogo.campeonato.nome}</td>
												<td>${cartaoVermelho.tempo} minuto(s)</td>
											</tr>
										</c:forEach>
									</c:if>
									<c:if test="${empty listaVermelhos}">
										<tr class="table-align-middle">
											<td colspan="3">Nenhum cartão vermelho registrado</td>
										</tr>
									</c:if>
								</tbody>
							</table>
						</div>
					</div>
				</section>
			</div>
		</div>
		<div class="row">
			<div class="col text-center">
				<a href="#" onclick="history.go(-1)" class="btn btn-secondary btn-lg mt-3">Voltar</a>
			</div>
		</div>
	</div>
	<!-- Fim Container -->

	<!-- Modal Login -->
	<c:import url="../login-modal.jsp" />
	<!-- footer -->
	<footer id="footer" class="bg-dark py-3 flex-shrink-0">
		<div class="container">
			<div class="row flex-column align-items-center">
				<div class="col-6 text-center">
					<a href="#" class="btn"> <i
						class="fab fa-facebook fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-twitter fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-instagram fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-google-plus fa-3x text-white m-2"></i>
					</a>
				</div>
				<div class="col-6">
					<p class="text-center text-bold text-white">2018 © sixTeam</p>
				</div>
			</div>
		</div>
	</footer>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
		crossorigin="anonymous"></script>
	<script
		src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js"
		integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9"
		crossorigin="anonymous"></script>
	
	<script>
		$(document).ready(function() {
			$('body').bootstrapMaterialDesign();
			//let params = new URLSearchParams(window.location.search);
		});
	</script>
</body>
</html>