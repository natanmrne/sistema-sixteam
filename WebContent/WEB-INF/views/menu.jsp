<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- NAVBAR -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-success">
    <a class="navbar-brand" href="/sixteam/inicio">sixTeam</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-between" id="navbarNavDropdown">
      <ul class="navbar-nav ulclass">
      	<c:if test="${usuario != null}">
        <li class="nav-item">
        	<a class="nav-link" href="/sixteam/administracao">Administração</a>
        </li>
        </c:if>
        <!-- CAMPEONATOS -->
        <li class="nav-item">
          <a class="nav-link" href="/sixteam/campeonatos">Campeonatos</a>
        </li>
        <!-- FIM CAMPEONATOS -->
        <!-- EQUIPES -->
        <li class="nav-item dropdown">
          <a class="nav-link" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Equipes
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a href="/sixteam/equipes" class="btn w-100 mb-0">Equipes</a>
            <a href="/sixteam/selecao_campeonato?tipo=equipes" class="btn w-100 mb-0">Equipes por Campeonato</a>
          </div>
        </li>
        <!-- FIM EQUIPES -->
        <!-- JOGADORES -->
        <li class="nav-item dropdown">
          <a class="nav-link" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Jogadores
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a href="/sixteam/jogadores" class="btn w-100 mb-0">Jogadores</a>
            <a href="/sixteam/selecao_equipe?tipo=jogadores" class="btn w-100 mb-0">Jogadores por Equipe</a>
          </div>
        </li>
        <!-- FIM JOGADORES -->
        <!-- JOGOS -->
        <li class="nav-item dropdown">
          <a class="nav-link" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Jogos
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a href="/sixteam/selecao_campeonato?tipo=jogos" class="btn w-100 mb-0">Jogos por Campeonato</a>
          </div>
        </li>
        <!-- FIM JOGOS -->
        <!-- TREINADORES -->
        <li class="nav-item">
          <a class="nav-link" href="/sixteam/treinadores">Treinadores</a>
        </li>
        <!-- FIM TREINADORES -->
        <!-- ÁRBITROS -->
        <li class="nav-item">
          <a class="nav-link" href="/sixteam/arbitros">Árbitros</a>
        </li>
        <!-- FIM ÁRBITROS -->
      </ul>
      <!-- LOGIN -->
      <c:if test="${usuario == null}">
	      <ul class="navbar-nav">
	        <li class="nav-item">
	          <a class="nav-link" href="#" data-toggle="modal" data-target="#loginModal">Login</a>
	        </li>
	      </ul>
      </c:if>
      <!-- FIM LOGIN -->
      <!-- USUÁRIO LOGADO -->
      <c:if test="${usuario != null}">
	      <ul class="navbar-nav">
	        <li class="nav-item">
	          <div class="d-flex align-items-center justify-content-center">
	            <p class="my-auto mr-2 text-white">Logado como: ${usuario.usuario}</p>
	            <a class="btn-sair" href="/sixteam/fazer_logout">
	              <i class="fas fa-sign-out-alt fa-2x"></i>
	            </a>
	          </div>
	        </li>
	      </ul>
      </c:if>
      <!-- FIM USUÁRIO LOGADO -->
    </div>
  </nav>
  <!-- FIM DO NAVBAR -->