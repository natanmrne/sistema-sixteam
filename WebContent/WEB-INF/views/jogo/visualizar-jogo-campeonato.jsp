<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<c:import url="../head.jsp" />
</head>
<body>
	<!-- NAVBAR START -->
	<c:import url="../menu.jsp" />
	<!-- NAVBAR END -->
	<!-- DADOS -->
		<!-- Gols Time Casa -->
		<c:set var="golsCasa" value="${gols.stream().filter( i -> Integer.valueOf(jogo.timeCasa.id).equals(i.getTime().getId())).count() }"/>
		<!--  Gols Time Visitante -->
		<c:set var="golsVisitante" value="${gols.stream().filter( i -> Integer.valueOf(jogo.timeVisitante.id).equals(i.getTime().getId())).count() }"/>
		
		<!-- Faltas Time Casa -->
		<c:set var="faltasCasa" value="${faltas.stream().filter( i -> Integer.valueOf(jogo.timeCasa.id).equals(i.getTime().getId())).count() }"/>
		<!-- Faltas Time Visitante -->
		<c:set var="faltasVisitante" value="${faltas.stream().filter( i -> Integer.valueOf(jogo.timeVisitante.id).equals(i.getTime().getId())).count() }"/>
		
		<!-- Cartões Amarelos Casa -->
		<c:set var="caCasa" value="${cartoesAmarelos.stream().filter( i -> Integer.valueOf(jogo.timeCasa.id).equals(i.getTime().getId())).count() }"/>
		<!-- Cartões Amarelos Visitante -->
		<c:set var="caVisitante" value="${cartoesAmarelos.stream().filter( i -> Integer.valueOf(jogo.timeVisitante.id).equals(i.getTime().getId())).count() }"/>
		
		<!-- Cartões Vermelhos Casa -->
		<c:set var="cvCasa" value="${cartoesVermelhos.stream().filter( i -> Integer.valueOf(jogo.timeCasa.id).equals(i.getTime().getId())).count() }"/>
		<!-- Cartões Vermelhos Visitante -->
		<c:set var="cvVisitante" value="${cartoesVermelhos.stream().filter( i -> Integer.valueOf(jogo.timeVisitante.id).equals(i.getTime().getId())).count() }"/>
		
		<!-- Substituições Casa -->
		<c:set var="subsCasa" value="${substituicoes.stream().filter( i -> Integer.valueOf(jogo.timeCasa.id).equals(i.getTime().getId())).count() }"/>
		<!-- Substituições Visitante -->
		<c:set var="subsVisitante" value="${substituicoes.stream().filter( i -> Integer.valueOf(jogo.timeVisitante.id).equals(i.getTime().getId())).count() }"/>
	<!-- FIM DADOS -->
	<!-- Container -->
	<div class="container py-5 size">
		<div class="row">
			<div class="col mb-3">
				<h1 class="title text-center">Jogo #${jogo.id}</h1>
				<div class="title-underline bg-success"></div>
				<p class="text-center font-italic font-weight-light my-3 display-6">Informações
					do jogo</p>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="card">
					<div class="card-body">
						<div
							class="row justify-content-between align-items-center flex-wrap">
							<div class="col-lg-4">
								<div class="bandeira">
									<h6 class="font-italic text-secondary">Time da Casa</h6>
									<img class="img-fluid img-thumbnail" src="/sixteam/${jogo.timeCasa.time.bandeira}" alt="">
									<h5>
										<a href="/sixteam/equipes/visualizar/${jogo.timeCasa.id}" style="color: black;">${jogo.timeCasa.time.nome}</a>
									</h5>
								</div>
							</div>
							<div class="col-lg-4">
								<div
									class="score text-center d-flex flex-column flex-lg-row justify-content-around align-items-center">
									<h1 class="my-auto">
										<c:out value="${golsCasa}" />
									</h1>
									<h5 class="my-auto">X</h5>
									<h1 class="my-auto">
										<c:out value="${golsVisitante}" />
									</h1>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="bandeira flex-column-reverse flex-lg-column">
									<h6 class="font-italic text-secondary mt-3 mt-lg-0">Time
										Visitante</h6>
									<img class="img-fluid img-thumbnail" src="/sixteam/${jogo.timeVisitante.time.bandeira}" alt="">
									<h5 class="mb-3 mb-lg-0">
										<a href="/sixteam/equipes/visualizar/${jogo.timeVisitante.id}" style="color: black;">${jogo.timeVisitante.time.nome}</a>
									</h5>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row mt-5 justify-content-center">
			<div class="col-12 col-lg-6">
				<div class="card">
					<div class="card-header text-center py-0">
						<h4 class="my-3">Informações</h4>
					</div>
					<div class="card-body">
						<div class="row justify-content-center">
							<div class="col 6 text-right font-weight-bold estatisticas">
								<p>Campeonato</p>
								<p>Turno</p>
								<p>Rodada</p>
								<p>Data</p>
								<p>Hora</p>
								<p>Local</p>
								<p>Árbitro</p>
							</div>
							<div class="col 6 text-left estatisticas">
								<p>${jogo.campeonato.nome}</p>
								<p>
									<c:forTokens var="turno" items="1,2,8ª de Final,4ª de Final,Semi-Final,Final" delims="," varStatus="t">
										<c:if test="${ jogo.turno == t.index+1 }">
											${turno}
										</c:if>
									</c:forTokens>
								</p>
								<p>${jogo.rodada}</p>
								<p><fmt:formatDate value="${jogo.data}" pattern="dd/MM/yyyy"/></p>
								<p><fmt:formatDate value="${jogo.hora}" pattern="HH:mm"/></p>
								<p>${jogo.local.nome}</p>
								<p>${jogo.arbitro.nome}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-lg-6">
				<div class="card my-auto">
					<div class="card-header text-center py-0">
						<h4 class="my-3">Súmula</h4>
					</div>
					<div class="card-body">
						<div class="row justify-content-center">
							<div class="col-3 flex-column text-right estatisticas">
								<a href="/sixteam/equipes/visualizar/${jogo.timeCasa.id}" class="text-success">
									${jogo.timeCasa.time.nome}
								</a>
								<p><c:out value="${golsCasa}" /></p>
								<p><c:out value="${faltasCasa}" /></p>
								<p><c:out value="${caCasa}" /></p>
								<p><c:out value="${cvCasa}" /></p>
								<p><c:out value="${subsCasa}" /></p>
							</div>
							<div class="col-6">
								<div class="row justify-content-center">
									<div class="col d-flex flex-column text-center estatisticas font-weight-bold est">
										<p class="text-success">Equipe</p>
										<p>Gols</p>
										<p>Faltas</p>
										<p>Cartões Amarelos</p>
										<p>Cartões Vermelhos</p>
										<p>Substituições</p>
									</div>
								</div>
							</div>
							<div class="col-3 flex-column text-left estatisticas">
								<a href="/sixteam/equipes/visualizar/${jogo.timeVisitante.id}" class="text-success">${jogo.timeVisitante.time.nome}</a>
								<p><c:out value="${golsVisitante}" /></p>
								<p><c:out value="${faltasVisitante}" /></p>
								<p><c:out value="${caVisitante}" /></p>
								<p><c:out value="${cvVisitante}" /></p>
								<p><c:out value="${subsVisitante}" /></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<section id="gols">
			<div class="row mt-5">
				<div class="col">
					<h1 class="title text-center">Gols</h1>
					<div class="title-underline bg-success"></div>
				</div>
			</div>
			<div class="row mt-2 justify-content-center">
				<div class="table-responsive-md mt-3 w-100">
					<table class="table text-center">
						<thead class="thead-dark">
							<tr>
								<th scope="col">#</th>
								<th scope="col">Equipe</th>
								<th scope="col">Jogador</th>
								<th scope="col">Tipo de Gol</th>
								<th scope="col">Tempo</th>
							</tr>
						</thead>
						<tbody>
							<c:if test="${not empty gols}" >
								<c:forEach var="gol" items="${gols}">
									<tr class="table-align-middle">
										<th scope="row">${gol.id}</th>
										<td>${gol.time.time.nome}</td>
										<td><a href="/sixteam/jogadores/visualizar/${gol.jogador.id}" class="text-success">${gol.jogador.nome}</a></td>
										<td>${gol.acao.descricao}</td>
										<td>${gol.tempo} minuto(s)</td>
									</tr>
								</c:forEach>
							</c:if>
							<c:if test="${empty gols}">
								<tr class="table-align-middle">
									<td colspan="6">Nenhum gol registrado</td>
								</tr>
							</c:if>
						</tbody>
					</table>
				</div>
			</div>
		</section>
		<section id="amarelos">
			<div class="row mt-5">
				<div class="col">
					<h1 class="title text-center">Cartões Amarelos</h1>
					<div class="title-underline bg-success"></div>
				</div>
			</div>
			<div class="row mt-2 justify-content-center">
				<div class="table-responsive-md mt-3 w-100">
					<table class="table text-center">
						<thead class="thead-dark">
							<tr>
								<th scope="col">#</th>
								<th scope="col">Equipe</th>
								<th scope="col">Jogador</th>
								<th scope="col">Tempo</th>
							</tr>
						</thead>
						<tbody>
							<c:if test="${not empty cartoesAmarelos}">
								<c:forEach var="cartaoAmarelo" items="${cartoesAmarelos}">
									<tr class="table-align-middle">
										<th scope="row">${cartaoAmarelo.id}</th>
										<td>${cartaoAmarelo.time.time.nome}</td>
										<td><a href="/sixteam/jogadores/visualizar/${cartaoAmarelo.jogador.id}" class="text-success">${cartaoAmarelo.jogador.nome}</a></td>
										<td>${cartaoAmarelo.tempo} minuto(s)</td>
									</tr>
								</c:forEach>
							</c:if>
							<c:if test="${empty cartoesAmarelos}">
								<tr class="table-align-middle">
									<td colspan="6">Nenhum cartão amarelo registrado</td>
								</tr>
							</c:if>
						</tbody>
					</table>
				</div>
			</div>
		</section>
		<section id="vermelhos">
			<div class="row mt-5">
				<div class="col">
					<h1 class="title text-center">Cartões Vermelhos</h1>
					<div class="title-underline bg-success"></div>
				</div>
			</div>
			<div class="row mt-2 justify-content-center">
				<div class="table-responsive-md mt-3 w-100">
					<table class="table text-center">
						<thead class="thead-dark">
							<tr>
								<th scope="col">#</th>
								<th scope="col">Equipe</th>
								<th scope="col">Jogador</th>
								<th scope="col">Tempo</th>
							</tr>
						</thead>
						<tbody>
							<c:if test="${not empty cartoesVermelhos}">
								<c:forEach var="cartaoVermelho" items="${cartoesVermelhos}">
									<tr class="table-align-middle">
										<th scope="row">${cartaoVermelho.id}</th>
										<td>${cartaoVermelho.time.time.nome}</td>
										<td><a href="/sixteam/jogadores/visualizar/${cartaoVermelho.jogador.id}" class="text-success">${cartaoVermelho.jogador.nome}</a></td>
										<td>${cartaoVermelho.tempo} minuto(s)</td>
									</tr>
								</c:forEach>
							</c:if>
							<c:if test="${empty cartoesVermelhos}">
								<tr class="table-align-middle">
									<td colspan="6">Nenhum cartão vermelho registrado</td>
								</tr>
							</c:if>
						</tbody>
					</table>
				</div>
			</div>
			<div class="row">
				<div class="col text-center">
					<a href="#" onclick="history.go(-1)" class="btn btn-secondary btn-lg mt-3">Voltar</a>
				</div>
			</div>
		</section>
	</div>
	<!-- Fim Container -->

	<!-- Modal Login -->
	<c:import url="../login-modal.jsp" />
	<!-- footer -->
	<footer id="footer" class="bg-dark py-3 flex-shrink-0">
		<div class="container">
			<div class="row flex-column align-items-center">
				<div class="col-6 text-center">
					<a href="#" class="btn"> <i
						class="fab fa-facebook fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-twitter fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-instagram fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-google-plus fa-3x text-white m-2"></i>
					</a>
				</div>
				<div class="col-6">
					<p class="text-center text-bold text-white">2018 © sixTeam</p>
				</div>
			</div>
		</div>
	</footer>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
		crossorigin="anonymous"></script>
	<script
		src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js"
		integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9"
		crossorigin="anonymous"></script>
	
	<script>
		$(document).ready(function() {
			$('body').bootstrapMaterialDesign();
			//let params = new URLSearchParams(window.location.search);
		});
	</script>
</body>
</html>