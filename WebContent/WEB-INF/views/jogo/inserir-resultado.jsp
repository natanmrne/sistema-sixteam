<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<c:import url="../head.jsp" />
</head>
<body>
	<!-- NAVBAR START -->
	<c:import url="../menu.jsp" />
	<!-- NAVBAR END -->
	<!-- Container -->
	<div class="container p-5 size">
		<p>${estatistica}</p>
		<div class="row">
			<div class="col mb-3">
				<h1 class="title text-center">Inserir Evento no Jogo #${jogo.id}</h1>
				<div class="title-underline bg-success"></div>
				<p class="text-center font-italic font-weight-light my-3 display-6">Insira um evento ocorrido
					no jogo de ${jogo.timeCasa.time.nome} x ${jogo.timeVisitante.time.nome}</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md">
				<div class="card">
					<div class="card-header py-1 my-0">
						<h5 class="text-center m-0">Dados</h5>
					</div>
					<div class="card-body">
						<form action="/sixteam/jogo/inserir_resultado" method="post">
							<input type="hidden" name="jogo.id" value="${jogo.id}" />
							<div class="form">
								<div class="form-row">
									<div class="col">
										<div class="form-group">
											<label for="acao" class="bmd-label-floating">Tipo do Evento</label>
											<select id="acao" name="acao.id" class="form-control" style="height: 2rem;">
												<c:forEach var="acao" items="${acoes}">
													<option value="${acao.id}">${acao.descricao}</option>
												</c:forEach>
											</select>
										</div>
									</div>
								</div>
								<div class="form-row">
									<div class="col">
										<div class="form-group">
											<label for="equipe">Equipe</label>
											<select id="equipe" name="time.id" class="form-control" style="height: 2rem;">
												<c:forEach var="time" items="${times}">
													<option value="${time.id}">${time.time.nome}</option>
												</c:forEach>
											</select>
										</div>
									</div>
									<div class="col">
										<div class="form-group">
											<label for="jogador">Jogador</label>
											<select id="jogador" name="jogador.id" class="form-control" style="height: 2rem;">
												<c:forEach var="timeJogador" items="${jogadores}">
													<option value="${timeJogador.jogador.id}" data-tag="${timeJogador.time.id}">${timeJogador.jogador.nome}</option>
												</c:forEach>
											</select>
										</div>
									</div>
								</div>
								<div class="form-row">
									<div class="col">
										<div class="form-group">
											<label for="tempo">Tempo</label> <input type="number"
												id="tempo" name="tempo" class="form-control" min="0"
												max="120" value="0" onKeyPress="return false;">
										</div>
									</div>
								</div>
								<div class="form-row">
									<div class="col d-flex justify-content-center">
										<div class="form-group mb-0">
											<button type="submit" class="btn btn-success btn-lg my-0">Inserir</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Fim Container -->

	<!-- Modal Login -->
	<c:import url="../login-modal.jsp" />
	<!-- footer -->
	<footer id="footer" class="bg-dark py-3 flex-shrink-0">
		<div class="container">
			<div class="row flex-column align-items-center">
				<div class="col-6 text-center">
					<a href="#" class="btn"> <i
						class="fab fa-facebook fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-twitter fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-instagram fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-google-plus fa-3x text-white m-2"></i>
					</a>
				</div>
				<div class="col-6">
					<p class="text-center text-bold text-white">2018 © sixTeam</p>
				</div>
			</div>
		</div>
	</footer>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
		crossorigin="anonymous"></script>
	<script
		src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js"
		integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9"
		crossorigin="anonymous"></script>
	
	<!-- Datepicker JS -->
	<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js"
		type="text/javascript"></script>
	<script>
		$(document).ready(function() {
			$('body').bootstrapMaterialDesign();
			const selected = ${times.get(0).getId()};
			$("#jogador option").each(function(item){
				let element =  $(this); 
				if (element.data("tag") != selected){
					element.removeClass('visible');
		            element.addClass('hidden');
					element.hide();
				}
			});
			
		});
	</script>
	<script>
		$('#equipe').on('change', function() {
		    let selected = $(this).val();
		    $("#jogador option").each(function(){
		        let element =  $(this) ;
		        if (element.data("tag") != selected){
		            element.removeClass('visible');
		            element.addClass('hidden');
		            element.hide() ;
		        }else{
		            element.removeClass('hidden');
		            element.addClass('visible');
		            element.show();
		        }
		    });
		    let jogador = $('#jogador');
		    jogador.prop('selectedIndex', jogador.find("option.visible:eq(0)").index());
		}).triggerHandler('change');
	</script>
</body>
</html>