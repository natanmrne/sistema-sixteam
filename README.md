# Sistema sixTeam

## Descrição

> Sistema para gerenciamento de campeonatos de futebol, desenvolvido com SpringMVC, HTML5, CSS3, Material Design for Bootstrap e Javascript.

## Informações do Grupo

> **Unidade:** USJT Mooca **Curso:** Análise e Desenvolvimento de Sistemas **Turma:** ADS2BN-MCA2
> - Natan Morone Nunes `RA: 81720664`
> - Pedro Bachiega Neto `RA: 817120762`
> - Helio Pessanha `RA: 817122300`
> - André Gianfratti  `RA: 817114511`
> - Raul Machado `RA: 817124921`
> - Estevan `RA: 817124710`
