package br.usjt.ads.sixteam.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="regra")
public class Regra {
	@Id
	@Column(name="id")
	@NotNull
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@NotNull
	@Column(name="quantidade_times")
	private int quantidadeTimes;
	@NotNull
	@Column(name="quantidade_grupos")
	private int quantidadeGrupos;
	@NotNull
	@Column(name="pontos_vitoria")
	private int pontosVitoria;
	@NotNull
	@Column(name="pontos_derrota")
	private int pontosDerrota;
	@NotNull
	@Column(name="pontos_empate")
	private int pontosEmpate;
	@NotNull
	@Column(name="pontos_empate_zero")
	private int pontosEmpateZero;
	@NotNull
	@Column(name="tempo_jogo")
	private int tempoJogo;
	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name="data_inicio")
	private Date dataInicio;
	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name="data_termino")
	private Date dataTermino;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getQuantidadeTimes() {
		return quantidadeTimes;
	}
	public void setQuantidadeTimes(int quantidadeTimes) {
		this.quantidadeTimes = quantidadeTimes;
	}
	public int getQuantidadeGrupos() {
		return quantidadeGrupos;
	}
	public void setQuantidadeGrupos(int quantidadeGrupos) {
		this.quantidadeGrupos = quantidadeGrupos;
	}
	public int getPontosVitoria() {
		return pontosVitoria;
	}
	public void setPontosVitoria(int pontosVitoria) {
		this.pontosVitoria = pontosVitoria;
	}
	public int getPontosDerrota() {
		return pontosDerrota;
	}
	public void setPontosDerrota(int pontosDerrota) {
		this.pontosDerrota = pontosDerrota;
	}
	public int getPontosEmpate() {
		return pontosEmpate;
	}
	public void setPontosEmpate(int pontosEmpate) {
		this.pontosEmpate = pontosEmpate;
	}
	public int getPontosEmpateZero() {
		return pontosEmpateZero;
	}
	public void setPontosEmpateZero(int pontosEmpateZero) {
		this.pontosEmpateZero = pontosEmpateZero;
	}
	public int getTempoJogo() {
		return tempoJogo;
	}
	public void setTempoJogo(int tempoJogo) {
		this.tempoJogo = tempoJogo;
	}
	public Date getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public Date getDataTermino() {
		return dataTermino;
	}
	public void setDataTermino(Date dataTermino) {
		this.dataTermino = dataTermino;
	}
	
	@Override
	public String toString() {
		return "Regra [id=" + id + ", quantidadeTimes=" + quantidadeTimes + ", pontosVitoria=" + pontosVitoria
				+ ", pontosDerrota=" + pontosDerrota + ", pontosEmpate=" + pontosEmpate + ", pontosEmpateZero="
				+ pontosEmpateZero + ", tempoJogo=" + tempoJogo + ", dataInicio=" + dataInicio + ", dataTermino="
				+ dataTermino + "]";
	}
	
	
}
