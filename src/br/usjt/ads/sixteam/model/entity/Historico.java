package br.usjt.ads.sixteam.model.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="historico")
public class Historico {
	@Id
	@Column(name="id")
	@NotNull
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@NotNull
	@Column(name="tipo")
	private String tipo;
	@NotNull
	@Column(name="descricao")
	private String descricao;
	@NotNull
	@Column(name="id_usuario")
	private int idUsuario;
	@NotNull
	@Column(name="data")
	private Timestamp data;
	
	public Historico(String tipo, String descricao, int idUsuario) {
		setTipo(tipo);
		setDescricao(descricao);
		setIdUsuario(idUsuario);
		setData(new Timestamp(System.currentTimeMillis()));
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Timestamp getData() {
		return data;
	}
	public void setData(Timestamp data) {
		this.data = data;
	}
	
	
}
