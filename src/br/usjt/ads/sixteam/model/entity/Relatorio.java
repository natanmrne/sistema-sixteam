package br.usjt.ads.sixteam.model.entity;

import java.util.List;

import javax.persistence.Entity;

public class Relatorio {
	private List<Campeonato> campeonatos;
	private List<Time> times;
	private List<TimeCampeonato> timesCampeonato;
	private List<Jogador> jogadores;
	private List<TimeJogador> timesJogador;
	private List<Arbitro> arbitros;
	private List<Tecnico> tecnicos;
	private List<Local> locais;
	private List<Jogo> jogos;
	
	public List<Campeonato> getCampeonatos() {
		return campeonatos;
	}
	public void setCampeonatos(List<Campeonato> campeonatos) {
		this.campeonatos = campeonatos;
	}
	public List<Time> getTimes() {
		return times;
	}
	public void setTimes(List<Time> times) {
		this.times = times;
	}
	public List<TimeCampeonato> getTimesCampeonato() {
		return timesCampeonato;
	}
	public void setTimesCampeonato(List<TimeCampeonato> timesCampeonato) {
		this.timesCampeonato = timesCampeonato;
	}
	public List<Jogador> getJogadores() {
		return jogadores;
	}
	public void setJogadores(List<Jogador> jogadores) {
		this.jogadores = jogadores;
	}
	public List<TimeJogador> getTimesJogador() {
		return timesJogador;
	}
	public void setTimesJogador(List<TimeJogador> timesJogador) {
		this.timesJogador = timesJogador;
	}
	public List<Arbitro> getArbitros() {
		return arbitros;
	}
	public void setArbitros(List<Arbitro> arbitros) {
		this.arbitros = arbitros;
	}
	public List<Tecnico> getTecnicos() {
		return tecnicos;
	}
	public void setTecnicos(List<Tecnico> tecnicos) {
		this.tecnicos = tecnicos;
	}
	public List<Local> getLocais() {
		return locais;
	}
	public void setLocais(List<Local> locais) {
		this.locais = locais;
	}
	public List<Jogo> getJogos() {
		return jogos;
	}
	public void setJogos(List<Jogo> jogos) {
		this.jogos = jogos;
	}
}
