package br.usjt.ads.sixteam.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="time_campeonato")
public class TimeCampeonato {
	@Id
	@Column(name="id")
	@NotNull
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@NotNull
	@ManyToOne
	@JoinColumn(name="id_campeonato")
	private Campeonato campeonato;
	@NotNull
	@ManyToOne
	@JoinColumn(name="id_time")
	@Valid
	private Time time;
	@NotNull
	@ManyToOne
	@JoinColumn(name="id_grupo")
	private Grupo grupo;
	
	@NotNull
	@Column(name="pg")
	private int pg = 0;
	@NotNull
	@Column(name="jg")
	private int jg = 0;
	@NotNull
	@Column(name="vt")
	private int vt = 0;
	@NotNull
	@Column(name="em")
	private int em = 0;
	@NotNull
	@Column(name="dt")
	private int dt = 0;
	@NotNull
	@Column(name="gp")
	private int gp = 0;
	@NotNull
	@Column(name="gc")
	private int gc = 0;
	@NotNull
	@Column(name="sg")
	private int sg = 0;
	@NotNull
	@Column(name="ca")
	private int ca = 0;
	@NotNull
	@Column(name="cv")
	private int cv = 0;
	@NotNull
	@Column(name="pd")
	private int pd = 0;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Campeonato getCampeonato() {
		return campeonato;
	}
	public void setCampeonato(Campeonato campeonato) {
		this.campeonato = campeonato;
	}
	public Time getTime() {
		return time;
	}
	public void setTime(Time time) {
		this.time = time;
	}
	public Grupo getGrupo() {
		return grupo;
	}
	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}
	
	public int getPg() {
		return pg;
	}
	public void setPg(int pg) {
		this.pg = pg;
	}
	public int getJg() {
		return jg;
	}
	public void setJg(int jg) {
		this.jg = jg;
	}
	public int getVt() {
		return vt;
	}
	public void setVt(int vt) {
		this.vt = vt;
	}
	public int getEm() {
		return em;
	}
	public void setEm(int em) {
		this.em = em;
	}
	public int getDt() {
		return dt;
	}
	public void setDt(int dt) {
		this.dt = dt;
	}
	public int getGp() {
		return gp;
	}
	public void setGp(int gp) {
		this.gp = gp;
	}
	public int getGc() {
		return gc;
	}
	public void setGc(int gc) {
		this.gc = gc;
	}
	public int getSg() {
		return sg;
	}
	public void setSg(int sg) {
		this.sg = sg;
	}
	public int getCa() {
		return ca;
	}
	public void setCa(int ca) {
		this.ca = ca;
	}
	public int getCv() {
		return cv;
	}
	public void setCv(int cv) {
		this.cv = cv;
	}
	public int getPd() {
		return pd;
	}
	public void setPd(int pd) {
		this.pd = pd;
	}
	@Override
	public String toString() {
		return "TimeCampeonato [id=" + id + ", campeonato=" + campeonato + ", time=" + time + ", grupo=" + grupo
				+ ", pontos=" + "]";
	}
	
}
