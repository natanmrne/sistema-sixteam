package br.usjt.ads.sixteam.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="jogador")
public class Jogador {
	@Id
	@Column(name="id")
	@NotNull
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@NotNull
	@Size(min=4, max=45, message="Por favor digite um nome entre 4 e 45 caracteres")
	@Column(name="nome")
	private String nome;
	@NotNull
	@Size(min=5, max=45, message="Por favor digite uma URL para foto entre 5 e 45 caracteres")
	@Column(name="foto")
	private String foto;
	@NotNull
	@ManyToOne
	@JoinColumn(name="id_categoria")
	private Categoria categoria;
	@Column(name="disponivel")
	private boolean disponivel = true;
	@Column(name="ativo")
	private boolean ativo = true;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public boolean isAtivo() {
		return ativo;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	
	@Override
	public String toString() {
		return "Jogador [id=" + id + ", nome=" + nome + ", foto=" + foto + ", categoria=" + categoria + ", ativo="
				+ ativo + "]";
	}
	
	
}
