package br.usjt.ads.sixteam.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="jogo")
public class Jogo {
	@Id
	@Column(name="id")
	@NotNull
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name="data")
	private Date data;
	@NotNull
	@Column(name="hora")
	private java.sql.Time hora;
	@NotNull
	@Column(name="turno")
	private int turno;
	@NotNull
	@Column(name="rodada")
	private int rodada = 1;
	@NotNull
	@ManyToOne
	@JoinColumn(name="id_time_casa")
	private TimeCampeonato timeCasa;
	@NotNull
	@ManyToOne
	@JoinColumn(name="id_time_visitante")
	private TimeCampeonato timeVisitante;
	@NotNull
	@ManyToOne
	@JoinColumn(name="id_arbitro")
	private Arbitro arbitro;
	@NotNull
	@ManyToOne
	@JoinColumn(name="id_local")
	private Local local;
	@NotNull
	@ManyToOne
	@JoinColumn(name="id_campeonato")
	private Campeonato campeonato;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public java.sql.Time getHora() {
		return hora;
	}
	public void setHora(java.sql.Time hora) {
		this.hora = hora;
	}
	public int getTurno() {
		return turno;
	}
	public void setTurno(int turno) {
		this.turno = turno;
	}
	public int getRodada() {
		return rodada;
	}
	public void setRodada(int rodada) {
		this.rodada = rodada;
	}
	public TimeCampeonato getTimeCasa() {
		return timeCasa;
	}
	public void setTimeCasa(TimeCampeonato timeCasa) {
		this.timeCasa = timeCasa;
	}
	public TimeCampeonato getTimeVisitante() {
		return timeVisitante;
	}
	public void setTimeVisitante(TimeCampeonato timeVisitante) {
		this.timeVisitante = timeVisitante;
	}
	public Arbitro getArbitro() {
		return arbitro;
	}
	public void setArbitro(Arbitro arbitro) {
		this.arbitro = arbitro;
	}
	public Local getLocal() {
		return local;
	}
	public void setLocal(Local local) {
		this.local = local;
	}
	public Campeonato getCampeonato() {
		return campeonato;
	}
	public void setCampeonato(Campeonato campeonato) {
		this.campeonato = campeonato;
	}
	
	@Override
	public String toString() {
		return "Jogo [id=" + id + ", data=" + data + ", hora=" + hora + ", turno=" + turno + ", rodada=" + rodada
				+ ", timeCasa=" + timeCasa + ", timeVisitante=" + timeVisitante + ", arbitro=" + arbitro + ", local="
				+ local + ", campeonato=" + campeonato + "]";
	}
	
	
	
	
}
