package br.usjt.ads.sixteam.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="estatistica")
public class Estatistica {
	@Id
	@Column(name="id")
	@NotNull
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@NotNull
	@Column(name="tempo")
	private int tempo;
	@NotNull
	@ManyToOne
	@JoinColumn(name="id_acao")
	private Acao acao;
	@NotNull
	@ManyToOne
	@JoinColumn(name="id_time")
	private TimeCampeonato time;
	@NotNull
	@ManyToOne
	@JoinColumn(name="id_jogador")
	private Jogador jogador;
	@NotNull
	@ManyToOne
	@JoinColumn(name="id_jogo")
	private Jogo jogo;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getTempo() {
		return tempo;
	}
	public void setTempo(int tempo) {
		this.tempo = tempo;
	}
	public Acao getAcao() {
		return acao;
	}
	public void setAcao(Acao acao) {
		this.acao = acao;
	}
	public TimeCampeonato getTime() {
		return time;
	}
	public void setTime(TimeCampeonato time) {
		this.time = time;
	}
	public Jogador getJogador() {
		return jogador;
	}
	public void setJogador(Jogador jogador) {
		this.jogador = jogador;
	}
	public Jogo getJogo() {
		return jogo;
	}
	public void setJogo(Jogo jogo) {
		this.jogo = jogo;
	}
	
	@Override
	public String toString() {
		return "Estatistica [id=" + id + ", tempo=" + tempo + ", acao=" + acao + ", time=" + time + ", jogador="
				+ jogador + ", jogo=" + jogo + "]";
	}
	
	
}
