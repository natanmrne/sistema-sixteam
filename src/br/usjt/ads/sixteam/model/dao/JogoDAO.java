package br.usjt.ads.sixteam.model.dao;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.usjt.ads.sixteam.model.entity.Jogo;
import br.usjt.ads.sixteam.model.entity.TimeCampeonato;

@Repository
public class JogoDAO {
	@PersistenceContext
	EntityManager manager;
	
	public int inserir(Jogo jogo) throws IOException {
		manager.persist(jogo);
		return jogo.getId();
	}
	
	public void atualizar(Jogo jogo) throws IOException {
		manager.merge(jogo);
	}
	
	public void excluir(int id) throws IOException {
		manager.remove(manager.find(Jogo.class, id));
	}
	
	public Jogo buscar(int id) throws IOException {
		Query query = manager.createQuery("select j from Jogo j where j.id = :id");
		query.setParameter("id", id);
		return (Jogo) query.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<Jogo> buscarTodos() {
		return manager.createQuery("select j from Jogo j order by j.id asc").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Jogo> buscarTodosPorCampeonato(int idCampeonato) {
		Query query = manager.createQuery("select j from Jogo j where j.campeonato.id = :id");
		query.setParameter("id", idCampeonato);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Jogo> buscarTodosPorEquipe(TimeCampeonato tc) {
		Query query = manager.createQuery("select j from Jogo j where j.campeonato.id = :idC and j.timeCasa.id = :id or j.campeonato.id = :idC and j.timeVisitante.id = :id");
		query.setParameter("id", tc.getId());
		query.setParameter("idC", tc.getCampeonato().getId());
		return query.getResultList();
	}
	
}