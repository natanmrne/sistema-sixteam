package br.usjt.ads.sixteam.model.dao;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import br.usjt.ads.sixteam.model.entity.Local;


@Repository
public class LocalDAO {
	@PersistenceContext
	EntityManager manager;
	
	public int inserir(Local local) throws IOException {
		manager.persist(local);
		return local.getId();
	}
	
	public void atualizar(Local local) throws IOException {
		manager.merge(local);
	}
	
	public Local buscar(int id) throws IOException {
		return manager.find(Local.class, id);
	}
	
	public void excluir(int id) throws IOException {
		Query query = manager.createQuery("update Local l set l.ativo = false where l.id = :id");
		query.setParameter("id", id);
		query.executeUpdate();
	}
	
	@SuppressWarnings("unchecked")
	public List<Local> buscarTodos() throws IOException {
		return manager.createQuery("select l from Local l where l.ativo = true order by l.id asc").getResultList();
	}
}
