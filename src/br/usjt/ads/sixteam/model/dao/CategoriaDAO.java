package br.usjt.ads.sixteam.model.dao;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.usjt.ads.sixteam.model.entity.Categoria;

@Repository
public class CategoriaDAO {
	@PersistenceContext
	EntityManager manager;
	
	@SuppressWarnings("unchecked")
	public List<Categoria> buscarTodas() throws IOException {
		return manager.createQuery("select c from Categoria c order by c.id asc").getResultList();
	}
	
	public Categoria buscar(int id) {
		return manager.find(Categoria.class, id);
	}
	
}
