package br.usjt.ads.sixteam.model.dao;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import br.usjt.ads.sixteam.model.entity.Arbitro;


@Repository
public class ArbitroDAO {
	@PersistenceContext
	EntityManager manager;
	
	public int inserir(Arbitro arbitro) throws IOException {
		manager.persist(arbitro);
		return arbitro.getId();
	}
	
	public void atualizar(Arbitro arbitro) throws IOException {
		manager.merge(arbitro);
	}
	
	public Arbitro buscar(int id) throws IOException {
		return manager.find(Arbitro.class, id);
	}
	
	public void excluir(int id) throws IOException {
		Query query = manager.createQuery("update Arbitro a set a.ativo = false where a.id = :id");
		query.setParameter("id", id);
		query.executeUpdate();
	}
	
	@SuppressWarnings("unchecked")
	public List<Arbitro> buscarTodos() throws IOException {
		return manager.createQuery("select a from Arbitro a where a.ativo = true order by a.id asc").getResultList();
	}
}
