package br.usjt.ads.sixteam.model.dao;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.usjt.ads.sixteam.model.entity.Time;
import br.usjt.ads.sixteam.model.entity.TimeJogador;

@Repository
public class TimeJogadorDAO {
	@PersistenceContext
	EntityManager manager;
	
	public int inserir(TimeJogador timeJogador) throws IOException {
		manager.persist(timeJogador);
		return timeJogador.getId();
	}
	
	public void atualizar(TimeJogador timeJogador) throws IOException {
		manager.merge(timeJogador);
	}
	
	public void excluir(int id) throws IOException {
		manager.remove(manager.find(TimeJogador.class, id));
	}
	
	public TimeJogador buscar(int id) throws IOException {
		return manager.find(TimeJogador.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<TimeJogador> buscarTodos() throws IOException {
		return manager.createQuery("select tj from TimeJogador tj where tj.jogador.ativo = true order by tj.id desc").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<TimeJogador> buscarTodosTitularesPorEquipe(int id) throws IOException {
		Query query = manager.createQuery("select tj from TimeJogador tj where tj.time.id = :id and tj.jogador.ativo = true and tj.titular = true order by tj.numero asc");
		query.setParameter("id", id);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<TimeJogador> buscarTodosReservasPorEquipe(int id) throws IOException {
		Query query = manager.createQuery("select tj from TimeJogador tj where tj.time.id = :id and tj.jogador.ativo = true and tj.titular = false order by tj.numero asc");
		query.setParameter("id", id);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<TimeJogador> buscarTodosPorCampeonato(int idCampeonato) throws IOException {
		Query query = manager.createQuery("select tj from TimeJogador tj where tj.time.time.ativo = true and tj.campeonato.id = :id");
		query.setParameter("id", idCampeonato);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<TimeJogador> buscarTodosPorEquipe(int idEquipe) throws IOException {
		Query query = manager.createQuery("select tj from TimeJogador tj where tj.time.time.ativo = true and tj.time.id = :id");
		query.setParameter("id", idEquipe);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public TimeJogador buscarPorTimeEJogador(TimeJogador timeJogador) {
		Query query = manager.createQuery("select tj from TimeJogador tj where tj.time.id = :idT and tj.jogador.id = :idJ");
		query.setParameter("idT", timeJogador.getTime().getId());
		query.setParameter("idJ", timeJogador.getJogador().getId());
		List<TimeJogador> resultado = query.getResultList();
		if (resultado.size() > 0) {
			return (TimeJogador) query.getSingleResult();
		} else {
			return null;
		}
	}
	
	public void excluirTodosDeEquipe(Time time) {
		Query query = manager.createQuery("delete from TimeJogador tj where tj.time.id = :id");
		query.setParameter("id", time.getId());
		query.executeUpdate();
	}
}
