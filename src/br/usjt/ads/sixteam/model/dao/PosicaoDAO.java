package br.usjt.ads.sixteam.model.dao;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.usjt.ads.sixteam.model.entity.Posicao;

@Repository
public class PosicaoDAO {
	@PersistenceContext
	EntityManager manager;
	
	@SuppressWarnings("unchecked")
	public List<Posicao> buscarTodas() throws IOException {
		return manager.createQuery("select p from Posicao p order by p.id asc").getResultList();
	}
}
