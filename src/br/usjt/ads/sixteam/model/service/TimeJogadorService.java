package br.usjt.ads.sixteam.model.service;

import java.io.IOException;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.usjt.ads.sixteam.model.dao.TimeJogadorDAO;
import br.usjt.ads.sixteam.model.entity.Time;
import br.usjt.ads.sixteam.model.entity.TimeJogador;

@Service
public class TimeJogadorService {
	@Autowired
	private TimeJogadorDAO timeJogadorDAO;
	
	@Transactional
	public int inserir(TimeJogador timeJogador) throws IOException {
		return timeJogadorDAO.inserir(timeJogador);
	}
	
	@Transactional
	public void excluir(int id) throws IOException {
		timeJogadorDAO.excluir(id);
	}
	
	@Transactional
	public void atualizar(TimeJogador timeJogador) throws IOException {
		timeJogadorDAO.atualizar(timeJogador);
	}
	
	@Transactional
	public TimeJogador buscar(int id) throws IOException {
		return timeJogadorDAO.buscar(id);
	}
	
	@Transactional
	public List<TimeJogador> buscarTodos() throws IOException {
		return timeJogadorDAO.buscarTodos();
	}
	
	@Transactional
	public List<TimeJogador> buscarTodosPorCampeonato(int idCampeonato) throws IOException {
		return timeJogadorDAO.buscarTodosPorCampeonato(idCampeonato);
	}
	
	@Transactional
	public List<TimeJogador> buscarTodosPorEquipe(int idEquipe) throws IOException {
		return timeJogadorDAO.buscarTodosPorEquipe(idEquipe);
	}
	
	@Transactional
	public List<TimeJogador> buscarTodosTitularesPorEquipe(int id) throws IOException {
		return timeJogadorDAO.buscarTodosTitularesPorEquipe(id);
	}
	
	@Transactional
	public List<TimeJogador> buscarTodosReservasPorEquipe(int id) throws IOException {
		return timeJogadorDAO.buscarTodosReservasPorEquipe(id);
	}
	
	@Transactional
	public TimeJogador buscarPorTimeEJogador(TimeJogador timeJogador) throws IOException {
		return timeJogadorDAO.buscarPorTimeEJogador(timeJogador);
	}
	
	@Transactional
	public void excluirTodosDeEquipe(Time time) throws IOException {
		timeJogadorDAO.excluirTodosDeEquipe(time);
	}
}
