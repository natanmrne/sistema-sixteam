package br.usjt.ads.sixteam.model.service;

import java.io.IOException;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.usjt.ads.sixteam.model.dao.TecnicoDAO;
import br.usjt.ads.sixteam.model.entity.Tecnico;


@Service
public class TecnicoService {
	@Autowired
	private TecnicoDAO tecnicoDAO;
	
	@Transactional
	public int inserir(Tecnico tecnico) throws IOException {
		return tecnicoDAO.inserir(tecnico);
	}
	
	@Transactional
	public void atualizar(Tecnico tecnico) throws IOException {
		tecnicoDAO.atualizar(tecnico);
	}
	
	@Transactional 
	public Tecnico buscar(int id) throws IOException {
		return tecnicoDAO.buscar(id);
	}
	
	@Transactional
	public void excluir(int id) throws IOException {
		tecnicoDAO.excluir(id);
	}
	
	@Transactional
	public List<Tecnico> buscarTodos() throws IOException {
		return tecnicoDAO.buscarTodos();
	}
}
