package br.usjt.ads.sixteam.model.service;

import java.io.IOException;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.usjt.ads.sixteam.model.dao.TimeCampeonatoDAO;
import br.usjt.ads.sixteam.model.entity.TimeCampeonato;

@Service
public class TimeCampeonatoService {
	@Autowired
	private TimeCampeonatoDAO timeCampeonatoDAO;
	
	@Transactional
	public void inserir(TimeCampeonato timeCampeonato) throws IOException {
		timeCampeonatoDAO.inserir(timeCampeonato);
	}
	
	@Transactional
	public void excluir(int id) throws IOException {
		timeCampeonatoDAO.excluir(id);
	}
	
	@Transactional
	public void atualizar(TimeCampeonato timeCampeonato) throws IOException {
		timeCampeonatoDAO.atualizar(timeCampeonato);
	}
	
	@Transactional
	public TimeCampeonato buscar(int id) throws IOException {
		return timeCampeonatoDAO.buscar(id);
	}
	
	@Transactional
	public List<TimeCampeonato> buscarTodos() throws IOException {
		return timeCampeonatoDAO.buscarTodos();
	}
	
	@Transactional
	public List<TimeCampeonato> buscarTodosPorCampeonato(int idCampeonato) throws IOException {
		return timeCampeonatoDAO.buscarTodosPorCampeonato(idCampeonato);
	}
	
	@Transactional
	public TimeCampeonato buscarPorTimeECampeonato(int idTime, int idCampeonato) throws IOException {
		return timeCampeonatoDAO.buscarPorTimeECampeonato(idTime, idCampeonato);
	}
	
}
