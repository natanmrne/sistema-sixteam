package br.usjt.ads.sixteam.model.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.usjt.ads.sixteam.model.dao.GrupoDAO;
import br.usjt.ads.sixteam.model.entity.Grupo;
import br.usjt.ads.sixteam.model.entity.TimeCampeonato;

@Service
public class GrupoService {
	@Autowired
	private GrupoDAO grupoDAO;
	
	public List<Grupo> buscarListaGrupos(int quantidade) throws IOException {
		return grupoDAO.buscarListaGrupos(quantidade);
	}
	
	public List<Grupo> filtrar(List<Grupo> grupos, List<TimeCampeonato> timesCadastrados, int limite) {
		List<Grupo> novaLista = new ArrayList<Grupo>(grupos);
		int cont = 0;
		for (int i = 0; i < grupos.size(); i++) {
			for(TimeCampeonato timeCadastrado : timesCadastrados) {
				if (timeCadastrado.getGrupo().getId() == grupos.get(i).getId()) {
					cont++;
				}
			}
			if (cont == limite) {
				novaLista.remove(grupos.get(i));
			}
			cont = 0;
		}
		return novaLista;
	}
}
