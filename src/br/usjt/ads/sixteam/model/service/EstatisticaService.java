package br.usjt.ads.sixteam.model.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.usjt.ads.sixteam.model.dao.EstatisticaDAO;
import br.usjt.ads.sixteam.model.entity.Estatistica;
import br.usjt.ads.sixteam.model.entity.Jogo;
import br.usjt.ads.sixteam.model.entity.TimeCampeonato;
import br.usjt.ads.sixteam.model.entity.TimeJogador;


@Service
public class EstatisticaService {
	@Autowired
	private EstatisticaDAO estatisticaDAO;
	
	@Transactional
	public int inserir(Estatistica estatistica) throws IOException {
		return estatisticaDAO.inserir(estatistica);
	}
	
	@Transactional
	public Estatistica buscar(int id) throws IOException {
		return estatisticaDAO.buscar(id);
	}
	
	@Transactional
	public List<Estatistica> buscarGols(Jogo jogo) throws IOException {
		return estatisticaDAO.buscarGols(jogo);
	}
	
	@Transactional
	public List<Estatistica> buscarGols(TimeJogador jogador) throws IOException {
		return estatisticaDAO.buscarGols(jogador);
	}
	
	@Transactional
	public int buscarGols(TimeCampeonato time, Jogo jogo) throws IOException {
		return estatisticaDAO.buscarGols(time, jogo);
	}
	
	@Transactional
	public List<Estatistica> buscarFaltas(Jogo jogo) throws IOException {
		return estatisticaDAO.buscarFaltas(jogo);
	}
	
	@Transactional
	public List<Estatistica> buscarCA(Jogo jogo) throws IOException {
		return estatisticaDAO.buscarCA(jogo);
	}
	
	@Transactional
	public List<Estatistica> buscarCA(TimeJogador jogador) throws IOException {
		return estatisticaDAO.buscarCA(jogador);
	}
	
	@Transactional
	public List<Estatistica> buscarCA(TimeCampeonato time) throws IOException {
		return estatisticaDAO.buscarCA(time);
	}
	
	@Transactional
	public List<Estatistica> buscarCV(Jogo jogo) throws IOException {
		return estatisticaDAO.buscarCV(jogo);
	}
	
	@Transactional
	public List<Estatistica> buscarCV(TimeJogador jogador) throws IOException {
		return estatisticaDAO.buscarCV(jogador);
	}
	
	@Transactional
	public List<Estatistica> buscarCV(TimeCampeonato time) throws IOException {
		return estatisticaDAO.buscarCV(time);
	}
	
	@Transactional
	public List<Estatistica> buscarSubstituicoes(Jogo jogo) throws IOException {
		return estatisticaDAO.buscarSubstituicoes(jogo);
	}
	
	public boolean isVitoria(TimeCampeonato time, Jogo jogo) throws IOException {
		int golsCasa, golsVisitante;
		if(jogo.getTimeCasa().getId() == time.getId()) {
			golsCasa = buscarGols(time, jogo);
			golsVisitante = buscarGols(jogo.getTimeVisitante(), jogo);
			if(golsCasa > golsVisitante) {
				return true;
			} else {
				return false;
			}
		} else {
			golsCasa = buscarGols(jogo.getTimeCasa(), jogo);
			golsVisitante = buscarGols(time, jogo);
			if(golsVisitante > golsCasa) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	public boolean isEmpate(Jogo jogo) throws IOException {
		int golsCasa = buscarGols(jogo.getTimeCasa(), jogo);
		int golsVisitante = buscarGols(jogo.getTimeVisitante(), jogo);
		if(golsCasa == golsVisitante && golsCasa != 0) {
			return true;
		}
		return false;
	}
	
	public boolean isEmpateZero(Jogo jogo) throws IOException {
		int golsCasa = buscarGols(jogo.getTimeCasa(), jogo);
		int golsVisitante = buscarGols(jogo.getTimeVisitante(), jogo);
		if(golsCasa == golsVisitante && golsCasa == 0) {
			return true;
		}
		return false;
	}
	
	public List<TimeCampeonato> getTimesContra(TimeCampeonato time, List<Jogo> jogos) throws IOException {
		List<TimeCampeonato> lista = new ArrayList<TimeCampeonato>();
		for (Jogo j : jogos) {
			if(j.getTimeCasa().getId() == time.getId()) {
				if(!lista.contains(j.getTimeVisitante())) {
					lista.add(j.getTimeVisitante());
				}
			} else {
				if(!lista.contains(j.getTimeCasa())) {
					lista.add(j.getTimeCasa());
				}
			}
		}
		return lista;
	}
	
	
}
