package br.usjt.ads.sixteam.model.service;

import java.io.IOException;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.usjt.ads.sixteam.model.dao.HistoricoDAO;
import br.usjt.ads.sixteam.model.entity.Historico;

@Service
public class HistoricoService {
	
	@Autowired
	private HistoricoDAO historicoDAO;

	@Transactional
	public void inserir(Historico historico) throws IOException {
		historicoDAO.inserir(historico);
	}
	
}
