package br.usjt.ads.sixteam.model.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.usjt.ads.sixteam.model.dao.PosicaoDAO;
import br.usjt.ads.sixteam.model.entity.Posicao;

@Service
public class PosicaoService {
	@Autowired
	private PosicaoDAO posicaoDAO;
	
	public List<Posicao> buscarTodas() throws IOException {
		return posicaoDAO.buscarTodas();
	}
}
