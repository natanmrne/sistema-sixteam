package br.usjt.ads.sixteam.model.service;

import java.io.IOException;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.usjt.ads.sixteam.model.dao.LocalDAO;
import br.usjt.ads.sixteam.model.entity.Local;


@Service
public class LocalService {
	@Autowired
	private LocalDAO localDAO;
	
	@Transactional
	public int inserir(Local local) throws IOException {
		return localDAO.inserir(local);
	}
	
	@Transactional
	public void atualizar(Local local) throws IOException {
		localDAO.atualizar(local);
	}
	
	@Transactional 
	public Local buscar(int id) throws IOException {
		return localDAO.buscar(id);
	}
	
	@Transactional
	public void excluir(int id) throws IOException {
		localDAO.excluir(id);
	}
	
	@Transactional
	public List<Local> buscarTodos() throws IOException {
		return localDAO.buscarTodos();
	}
}
