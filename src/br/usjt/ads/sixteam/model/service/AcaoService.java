package br.usjt.ads.sixteam.model.service;

import java.io.IOException;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.usjt.ads.sixteam.model.dao.AcaoDAO;
import br.usjt.ads.sixteam.model.entity.Acao;


@Service
public class AcaoService {
	@Autowired
	private AcaoDAO acaoDAO;
	
	@Transactional 
	public Acao buscar(int id) throws IOException {
		return acaoDAO.buscar(id);
	}
	
	@Transactional
	public List<Acao> buscarTodas() throws IOException {
		return acaoDAO.buscarTodas();
	}
}
