package br.usjt.ads.sixteam.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.usjt.ads.sixteam.model.entity.Historico;
import br.usjt.ads.sixteam.model.entity.Local;
import br.usjt.ads.sixteam.model.entity.Usuario;
import br.usjt.ads.sixteam.model.service.HistoricoService;
import br.usjt.ads.sixteam.model.service.LocalService;

@Controller
public class LocalController {
	
	@Autowired
	private HistoricoService historicoService;
	@Autowired
	private LocalService localService;
	
	/*
	 ==============================================
	 =============Formata��o de Data===============
	 ==============================================
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	    dateFormat.setLenient(false);
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 ================Editar Local==================
	 ==============================================
	*/
	@RequestMapping("/locais/editar/{id}")
	public String telaEditarLocal(@PathVariable("id") int id, HttpSession session) {
		try {
			Local local = localService.buscar(id);
			session.setAttribute("local", local);
			return "local/editar-local";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/locais";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 ==============Cadastrar Local=================
	 ==============================================
	*/
	@RequestMapping("/local/cadastro")
	public String novoTreinador() {
		return "local/novo-local";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 ===============Lista de Locais================
	 ==============================================
	*/
	@RequestMapping("/locais")
	public String locales(HttpSession session) {
		List<Local> listaLocais;
		try {
			listaLocais = localService.buscarTodos();
			session.setAttribute("listaLocais", listaLocais);
			return "/local/lista-locais";
		} catch (IOException e) {
			session.setAttribute("listaLocais", null);
			e.printStackTrace();
		}
		return "redirect:/inicio";
	}
	
	// ========================================== CRUD ================================================================================
	
	/*
	 ==============================================
	 ===================CREATE=====================
	 ==============================================
	*/
	@RequestMapping("/inserir_local")
	public String inserirTreinador(@Valid Local local, BindingResult result, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		try {
			if(!result.hasFieldErrors()) {
				int idLocal = localService.inserir(local);
				historicoService.inserir(new Historico("Local", "Inclus�o #" + idLocal, usuario.getId()));
				return "redirect:/locais";
			} else {
				return "local/novo-local";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "index";
	}
	
	/*
	 ==============================================
	 ===================UPDATE=====================
	 ==============================================
	*/
	@RequestMapping("/atualizar_local")
	public String atualizarTreinador(@Valid Local local, BindingResult result, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		try {
			if(!result.hasFieldErrors()) {
				localService.atualizar(local);
				historicoService.inserir(new Historico("Local", "Atualiza��o #" + local.getId(), usuario.getId()));
				return "redirect:/locais";
			} else {
				return "local/editar-local";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/inicio";
	}
	
	/*
	 ==============================================
	 ===================DELETE=====================
	 ==============================================
	*/
	@RequestMapping("/local/desativar")
	public String excluirLocal(@RequestParam("id") int id, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		try {
			localService.excluir(id);
			historicoService.inserir(new Historico("Local", "Exclus�o #" + id, usuario.getId()));
			return "redirect:/locais";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/inicio";
	}
}
