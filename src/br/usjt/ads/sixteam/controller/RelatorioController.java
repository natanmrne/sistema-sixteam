package br.usjt.ads.sixteam.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import br.usjt.ads.sixteam.model.entity.Campeonato;
import br.usjt.ads.sixteam.model.entity.Relatorio;
import br.usjt.ads.sixteam.model.entity.Usuario;
import br.usjt.ads.sixteam.model.service.AcaoService;
import br.usjt.ads.sixteam.model.service.ArbitroService;
import br.usjt.ads.sixteam.model.service.CampeonatoService;
import br.usjt.ads.sixteam.model.service.EstatisticaService;
import br.usjt.ads.sixteam.model.service.JogadorService;
import br.usjt.ads.sixteam.model.service.JogoService;
import br.usjt.ads.sixteam.model.service.LocalService;
import br.usjt.ads.sixteam.model.service.LoginService;
import br.usjt.ads.sixteam.model.service.TecnicoService;
import br.usjt.ads.sixteam.model.service.TimeCampeonatoService;
import br.usjt.ads.sixteam.model.service.TimeJogadorService;
import br.usjt.ads.sixteam.model.service.TimeService;

@Controller
public class RelatorioController {
	
	@Autowired
	private CampeonatoService campeonatoService;
	@Autowired
	private TimeCampeonatoService timeCampeonatoService;
	@Autowired
	private TimeService timeService;
	@Autowired
	private TimeJogadorService timeJogadorService;
	@Autowired
	private JogadorService jogadorService;
	@Autowired
	private JogoService jogoService;
	@Autowired
	private ArbitroService arbitroService;
	@Autowired
	private TecnicoService tecnicoService;
	@Autowired
	private LocalService localService;
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value="/gerar_dados")
	public ResponseEntity<Relatorio> listarCampeonatos() {
		try {
			Relatorio relatorio = new Relatorio();
			relatorio.setCampeonatos(campeonatoService.buscarTodos());
			relatorio.setTimesCampeonato(timeCampeonatoService.buscarTodos());
			relatorio.setTimes(timeService.buscarTodos());
			relatorio.setTimesJogador(timeJogadorService.buscarTodos());
			relatorio.setJogadores(jogadorService.buscarTodos());
			relatorio.setJogos(jogoService.buscarTodos());
			relatorio.setArbitros(arbitroService.buscarTodos());
			relatorio.setTecnicos(tecnicoService.buscarTodos());
			relatorio.setLocais(localService.buscarTodos());
			return new ResponseEntity<Relatorio>(relatorio, HttpStatus.OK);
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
