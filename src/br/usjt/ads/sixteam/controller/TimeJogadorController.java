package br.usjt.ads.sixteam.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.usjt.ads.sixteam.model.entity.Historico;
import br.usjt.ads.sixteam.model.entity.TimeCampeonato;
import br.usjt.ads.sixteam.model.entity.TimeJogador;
import br.usjt.ads.sixteam.model.entity.Usuario;
import br.usjt.ads.sixteam.model.service.EstatisticaService;
import br.usjt.ads.sixteam.model.service.HistoricoService;
import br.usjt.ads.sixteam.model.service.JogadorService;
import br.usjt.ads.sixteam.model.service.PosicaoService;
import br.usjt.ads.sixteam.model.service.TimeCampeonatoService;
import br.usjt.ads.sixteam.model.service.TimeJogadorService;

@Controller
public class TimeJogadorController {
	
	@Autowired
	private JogadorService jogadorService;
	@Autowired
	private HistoricoService historicoService;
	@Autowired
	private PosicaoService posicaoService;
	@Autowired
	private TimeJogadorService timeJogadorService;
	@Autowired
	private TimeCampeonatoService timeCampeonatoService;
	@Autowired
	private EstatisticaService estatisticaService;
	
	/*
	 ==============================================
	 =============Formata��o de Data===============
	 ==============================================
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	    dateFormat.setLenient(false);
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 ===========Cadastrar Time/Jogador=============
	 ==============================================
	*/
	@RequestMapping("/registrar_jogador")
	public String novoTimeJogador(@RequestParam("time") int idTime, HttpSession session) {
		try {
			TimeCampeonato time = timeCampeonatoService.buscar(idTime);
			System.out.println("TIME >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + time);
			session.setAttribute("time", time);
			session.setAttribute("jogadoresEquipe", jogadorService.buscarPorCategoria(time.getTime().getCategoria().getId()));
			System.out.println("JOGADORES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + time.getTime().getCategoria().getId());
			session.setAttribute("posicoes", posicaoService.buscarTodas());
			return "jogador/novo-jogador-equipe";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/jogadores";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 =============Editar Time/Jogador==============
	 ==============================================
	*/
	@RequestMapping("/equipe/jogador/editar/{id}")
	public String telaEditarTimeJogador(@PathVariable("id") int idTimeJogador, HttpSession session) {
		try {
			session.setAttribute("timeJogador", timeJogadorService.buscar(idTimeJogador));
			session.setAttribute("posicoes", posicaoService.buscarTodas());
			return "jogador/editar-jogador-equipe";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 =============Visualizar Jogador===============
	 ==============================================
	*/
	@RequestMapping("/jogadores/visualizar/{id}")
	public String telaVisualizarJogador(@PathVariable("id") int idJogador, HttpSession session) {
		try {
			TimeJogador timeJogador = timeJogadorService.buscar(idJogador);
			session.setAttribute("timeJogador", timeJogador);
			System.out.println("GOLS >>>>>>>>>>>>>>>>>>>" + estatisticaService.buscarGols(timeJogador));
			session.setAttribute("listaGols", estatisticaService.buscarGols(timeJogador));
			session.setAttribute("listaAmarelos", estatisticaService.buscarCA(timeJogador));
			session.setAttribute("listaVermelhos", estatisticaService.buscarCV(timeJogador));
			return "jogador/visualizar-jogador-equipe";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/inicio";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 =============Lista Time/Jogador===============
	 ==============================================
	*/
	@RequestMapping("/jogadores/equipe/{id}")
	public String listaJogadoresPorEquipe(@PathVariable("id") int idTime, HttpSession session) throws IOException {
		
		try {
			session.setAttribute("time", timeCampeonatoService.buscar(idTime));
			session.setAttribute("jogadoresEquipe", timeJogadorService.buscarTodosPorEquipe(idTime));
			return "jogador/lista-jogadores-equipe";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 ===========Sele��o Time/Campeonato============
	 ==============================================
	*/
	@RequestMapping("/selecao_equipe")
	public String selecaoEquipe(@RequestParam("tipo") String tipo, HttpSession session) {
		List<TimeCampeonato> lista;
		try {
			lista = timeCampeonatoService.buscarTodos();
			session.setAttribute("listaEquipes", lista);
			session.setAttribute("tipo", tipo);
			return "equipe/selecao-equipe";
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return "redirect:/";
		
	}
	
	// ========================================== CRUD ================================================================================
	
	/*
	 ==============================================
	 ===================CREATE=====================
	 ==============================================
	*/
	@RequestMapping("/inserir_jogador_equipe")
	public String inserirTimeJogador(TimeJogador timeJogador, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		try {
			jogadorService.ocupar(timeJogador.getJogador().getId());
			timeJogadorService.inserir(timeJogador);
			historicoService.inserir(new Historico("Equipe/Jogador", "Inclus�o do Jogador #" + timeJogador.getJogador().getId() + " na Equipe #" + timeJogador.getTime().getId(), usuario.getId()));
			return "redirect:jogadores/equipe/" + timeJogador.getTime().getId();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/";
	}
	
	/*
	 ==============================================
	 ===================UPDATE=====================
	 ==============================================
	*/
	@RequestMapping("/atualizar_jogador_equipe")
	public String atualizarTimeJogador(TimeJogador timeJogador, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		try {
			timeJogadorService.atualizar(timeJogador);
			historicoService.inserir(new Historico("Equipe/Jogador", "Atualiza��o do Jogador #" + timeJogador.getJogador().getId() + " na Equipe #" + timeJogador.getTime().getId(), usuario.getId()));
			return "redirect:jogadores/equipe/" + timeJogador.getTime().getId();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/";
	}
	
	/*
	 ==============================================
	 ===================DELETE=====================
	 ==============================================
	*/
	@RequestMapping("/equipe/excluir_de_equipe")
	public String excluirJogadorDeEquipe(@RequestParam("id") int id, HttpSession session, HttpServletRequest request) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		String referer = request.getHeader("Referer");
		try {
			TimeJogador timeJogador =  timeJogadorService.buscar(id);
			jogadorService.desocupar(timeJogador.getJogador().getId());
			timeJogadorService.excluir(id);
			historicoService.inserir(new Historico("Equipe/Jogador", "Exclus�o do Jogador #" + id, usuario.getId()));
			return "redirect:" + referer;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/";
	}
}
